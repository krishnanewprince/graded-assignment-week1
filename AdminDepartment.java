package com.assignment.week1;

public class AdminDepartment extends SuperDepartment {
	public String departmentName() {
    	return "Admin Department";
    }
    public String work() {
    	return "Complete your documents submission";
    }
    public String deadline() {
    	return "Complete by EOD";
    }
}
