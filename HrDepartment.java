package com.assignment.week1;

public class HrDepartment extends SuperDepartment {
	public String departmentName() {
    	return "HR Department";
    }
   public String work() {
    	return "Fill todays worksheet and mark your attendance";
    }
    public String deadline() {
    	return "Complete by EOD";
    }
    public String activity() {
    	return "Team Lunch";
    }
}
