package com.assignment.week1;
import java.util.Scanner;
public class Main {
   public static void main(String args[]) {
	   // Creating objects
	   HrDepartment hrDepartment = new HrDepartment();
	   AdminDepartment adminDepartment = new AdminDepartment();
	   TechDepartment techDepartment = new TechDepartment();
	   
	   Scanner scan = new Scanner(System.in);
	   System.out.println("For HrDepartment press 1 \n" + "For AdminDepartment press 2 \n"
	   		+ "For TechDepartment press 3 ");
	   int count = scan.nextInt();
	   //Printing required department
	   while(count>0 && count<4) {
		   if(count == 1) {
			   System.out.println(hrDepartment.departmentName());
			   System.out.println(hrDepartment.work());
			   System.out.println(hrDepartment.deadline());
			   System.out.println(hrDepartment.activity());
			   System.out.println(hrDepartment.holiday());
		   }
		   if(count == 2) {
			   System.out.println(adminDepartment.departmentName());
			   System.out.println(adminDepartment.work());
			   System.out.println(adminDepartment.deadline());
			   System.out.println(adminDepartment.holiday());
		   }
		   if(count == 3) {
			   System.out.println(techDepartment.departmentName());
			   System.out.println(techDepartment.work());
			   System.out.println(techDepartment.deadline());
			   System.out.println(techDepartment.techStackInformation());
			   System.out.println(techDepartment.holiday());
		   }
		   System.out.println("\nPress 0 to exit \n" + "For HrDepartment press 1 \n" + "For AdminDepartment press 2 \n"
			   		+ "For TechDepartment press 3 ");
		   int exit = scan.nextInt();
			count = exit;
			 
	   }
	  System.out.println("loop ended"); 
   }
}
